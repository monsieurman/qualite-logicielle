package com.ipi.qualitelogicielle.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ipi.qualitelogicielle.model.Employe;

@Repository
public interface EmployeRepository extends JpaRepository<Employe, Long> {
    @Query("select max(substring(matricule,2)) from Employe")
    Optional<String> findLastMatricule();

    Optional<Employe> findByMatricule(String matricule);

    @Query("select avg(performance) from Employe where SUBSTRING(matricule,0,1) = ?1 ")
    Optional<Double> avgPerformanceWhereMatriculeStartsWith(String premiereLettreMatricule);
}
