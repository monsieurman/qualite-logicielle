package com.ipi.qualitelogicielle.service;

import com.ipi.qualitelogicielle.exception.EmployeException;
import com.ipi.qualitelogicielle.model.Employe;
import com.ipi.qualitelogicielle.model.Entreprise;
import com.ipi.qualitelogicielle.model.NiveauEtude;
import com.ipi.qualitelogicielle.model.Poste;
import com.ipi.qualitelogicielle.repository.EmployeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;

import java.time.LocalDate;

@Service
class EmployeService {

    public static final int MAX_MATRICULE_COUNT = 100000;
    @Autowired
    private EmployeRepository employeRepository;

    /**
     * Méthode enregistrant un nouvel employé dans l'entreprise
     *
     * @param nom          Le nom de l'employé
     * @param prenom       Le prénom de l'employé
     * @param poste        Le poste de l'employé
     * @param niveauEtude  Le niveau d'étude de l'employé
     * @param tempsPartiel Le pourcentage d'activité en cas de temps partiel
     * @throws EmployeException      Si on arrive au bout des matricules possibles
     * @throws EntityExistsException Si le matricule correspond à un employé existant
     */
    Employe embaucheEmploye(String nom, String prenom, Poste poste, NiveauEtude niveauEtude, Double tempsPartiel) throws EmployeException {
        //Récupération du type d'employé à partir du poste
        String typeEmploye = poste.name().substring(0, 1);
        //Récupération du dernier matricule...
        String lastMatricule = employeRepository.findLastMatricule()
                .orElse(Entreprise.MATRICULE_INITIAL);

        //... et incrémentation
        int numeroMatricule = Integer.parseInt(lastMatricule) + 1;
        if (numeroMatricule >= MAX_MATRICULE_COUNT) {
            throw new EmployeException("Limite des 100000 matricules atteinte !");
        }

        //On complète le numéro avec des 0 à gauche
        String matricule = "00000" + numeroMatricule;
        matricule = typeEmploye + matricule.substring(matricule.length() - 5);

        //On vérifie l'existence d'un employé avec ce matricule
        if (employeRepository.findByMatricule(matricule).isPresent()) {
            throw new EntityExistsException("L'employé de matricule " + matricule + " existe déjà en BDD");
        }

        //Calcul du salaire
        Double salaire = Entreprise.COEFF_SALAIRE_ETUDES.get(niveauEtude) * Entreprise.SALAIRE_BASE;
        if (tempsPartiel != null) {
            salaire *= tempsPartiel;
        }
        salaire = Math.round(salaire * 100.0) / 100.0;

        //Création et sauvegarde en BDD de l'employé.
        Employe employe = new Employe(nom, prenom, matricule, LocalDate.now(), salaire, Entreprise.PERFORMANCE_BASE, tempsPartiel);

        return employeRepository.save(employe);
    }


    /**
     * Méthode calculant la performance d'un commercial en fonction de ses objectifs et du chiffre d'affaire traité dans l'année.
     * Cette performance lui est affectée et sauvegardée en BDD
     * Si la performance ainsi calculée est supérieure à la moyenne des performances des commerciaux, il reçoit + 1 de performance.
     *
     * @param matricule  le matricule du commercial
     * @param caTraite   le chiffre d'affaire traité par le commercial pendant l'année
     * @param objectifCa l'object de chiffre d'affaire qui lui a été fixé
     * @throws EmployeException Si le matricule est null ou ne commence pas par un C
     */
    public Employe calculPerformanceCommercial(String matricule, Long caTraite, Long objectifCa) throws EmployeException {
        Employe employe = verifyNullConstraints(matricule, caTraite, objectifCa);
        int performance = getBasePerformance(caTraite, objectifCa, employe.getPerformance());
        performance += getCommercialPerformanceBonus(employe.getPerformance());

        // Affectation
        employe.setPerformance(performance);

        // Sauvegarde
        return employeRepository.save(employe);
    }

    /**
     * Calcul le bonus de performance d'un commercial en fonction de la moyenne.
     *
     * @param commercialPerformance La performance du commercial.
     * @return Le bonus de performance.
     */
    public Integer getCommercialPerformanceBonus(int commercialPerformance) {
        return employeRepository.avgPerformanceWhereMatriculeStartsWith("C")
                // If the employe is more performant than average
                .filter(avg -> avg < commercialPerformance)
                // He has a 1 bonus
                .map(avg -> 1)
                // Else, no bonus
                .orElse(0);
    }

    /**
     * Vérifie les contraintes d'intégrités avant de calculés les performances d'un commercial.
     *
     * @param matricule  Le matricule de l'employe.
     * @param caTraite   Le CA traité par l'entreprise.
     * @param objectifCa L'objectif de CA de l'entreprise.
     * @return L'employe sur lequel calculer la performance.
     * @throws EmployeException - Si le CA est null ou négatif.
     *                          - Si l'objectif CA est null ou négatif.
     *                          - Si le matricule est null.
     *                          - Si l'employé n'est pas un commercial.
     */
    private Employe verifyNullConstraints(String matricule, Long caTraite, Long objectifCa) throws EmployeException {
        //Vérification des paramètres d'entrée
        if (caTraite == null || caTraite < 0) {
            throw new EmployeException("Le chiffre d'affaire traité ne peut être négatif ou null !");
        }
        if (objectifCa == null || objectifCa < 0) {
            throw new EmployeException("L'objectif de chiffre d'affaire ne peut être négatif ou null !");
        }
        if (matricule == null || !isCommercial(matricule)) {
            throw new EmployeException("Le matricule ne peut être null et doit commencer par un C !");
        }
        //Recherche de l'employé dans la base
        return employeRepository.findByMatricule(matricule)
                .orElseThrow(() -> new EmployeException("Le matricule " + matricule + " n'existe pas !"));
    }

    private boolean isCommercial(String matricule) {
        return !matricule.isEmpty()
                && matricule.charAt(0) == 'C';
    }

    /**
     * Calcul la performance d'un commercial en y ajoutant un bonus en fonction de sa performance
     * vis à vis du CA de l'enreprise.
     *
     * @param caTraite   Le CA Traité par l'entreprise.
     * @param objectifCa L'objectif CA de l'entreprise.
     * @param employe    L'employe sur lequel calculer la performance.
     * @return La performance de l'employé.
     */
    public int getBasePerformance(Long caTraite, Long objectifCa, int employePerformance) {
        int performance;
        //Cas 2
        if (isBetweenIncl(caTraite, objectifCa * 0.8, objectifCa * 0.95)) {
            performance = Math.max(Entreprise.PERFORMANCE_BASE, employePerformance - 2);
        }
        //Cas 3
        else if (isBetweenIncl(caTraite, objectifCa * 0.95, objectifCa * 1.05)) {
            performance = Math.max(Entreprise.PERFORMANCE_BASE, employePerformance);
        }
        //Cas 4
        else if (isBetweenIncl(caTraite, objectifCa * 1.05, objectifCa * 1.2)) {
            performance = employePerformance + 1;
        }
        //Cas 5
        else if (caTraite > objectifCa * 1.2) {
            performance = employePerformance + 4;
        } else {
            performance = Entreprise.PERFORMANCE_BASE;
        }

        return performance;
    }

    /**
     * Check if a number is between two others. Inclusive.
     *
     * @param number The number to test.
     * @param min    The minimum cap.
     * @param max    The maximum cap.
     * @return True if the number is between the min and the max, inclusive.
     */
    // FIXME: move to utils
    private static boolean isBetweenIncl(double number, double min, double max) {
        return min <= number && number <= max;
    }
}
