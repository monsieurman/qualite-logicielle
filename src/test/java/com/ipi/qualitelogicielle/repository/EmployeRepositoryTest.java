package com.ipi.qualitelogicielle.repository;

import com.ipi.qualitelogicielle.model.Employe;
import lombok.val;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static com.ipi.qualitelogicielle.utils.ListUtils.from;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.params.provider.Arguments.arguments;

@SpringBootTest
class EmployeRepositoryTest {
    @Autowired
    private EmployeRepository repository;

    @AfterEach
    @BeforeEach
    void afterEach() {
        this.repository.deleteAll();
    }

    @ParameterizedTest
    @MethodSource("matriculeTestCases")
    void findLastMatricule(List<String> matriculeToInsert, Optional<String> expectedMatricule) {
        // Given
        matriculeToInsert.stream()
                .map(matricule -> Employe.builder().matricule(matricule).build())
                .forEach(repository::save);
        // When
        val lastMatricule = repository.findLastMatricule();
        // Then
        assertEquals(expectedMatricule, lastMatricule, "");
    }

    static Stream<Arguments> matriculeTestCases() {
        return Stream.of(
                arguments(Collections.emptyList(), Optional.empty()),
                arguments(from("M00001"), Optional.of("00001")),
                arguments(from("M00001", "T00003", "T00002"), Optional.of("00003"))
        );
    }
}

