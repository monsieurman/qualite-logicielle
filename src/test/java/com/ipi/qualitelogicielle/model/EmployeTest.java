package com.ipi.qualitelogicielle.model;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import lombok.val;

class EmployeTest {
    @ParameterizedTest
    @CsvSource({
            "2000-01-01, 2000-01-01, 0",
            "2000-01-01, 2001-01-01, 1",
            ",2000-01-01, 0",
            "2001-01-01, 2000-01-01, 0"
    })
    void getNombreAnneeAnciennete(LocalDate dateEmbauche, LocalDate now, int expectedYear) {
        // Given employe embauché en 2000
        val newEmploye = Employe.builder()
                .dateEmbauche(dateEmbauche)
                .build();
        // When getting nombre annee ancienneté 1 year after
        val anneeAnciennete = newEmploye.getNombreAnneeAnciennete(now);
        // Then it should return 1
        assertEquals(anneeAnciennete, expectedYear);
    }

    @ParameterizedTest
    @CsvSource({
            "T45632, 1, 0, 1.0, 1100",
            "T12345, 1, 2, 1.0, 2400",
            "M12345, 1, 0, 1, 1800"
    })
    void getPrimeAnnuelle(String matricule, int anneeAnciennete, int performance, Double tempsPartiel, Double expectedPrime) {
        val dateEmbauche = LocalDate.of(2000, 1, 1);
        val dateActuelle = dateEmbauche.plusYears(anneeAnciennete);
        // Given
        val employe = Employe.builder()
                .matricule(matricule)
                .dateEmbauche(dateEmbauche)
                .salaire(1.0)
                .performance(performance)
                .tempsPartiel(tempsPartiel)
                .build();
        // When
        Double prime = employe.getPrimeAnnuelle(dateActuelle);
        // Then
        assertEquals(expectedPrime, prime);
    }

    @ParameterizedTest
    @CsvSource({
            "M12345, true",
            "T12345, false"
    })
    void isManagerTest(String matricule, boolean expected) {
        // Given
        val employe = Employe.builder().matricule(matricule).build();
        // When / Then
        assertEquals(expected, employe.isManager());
    }

    @ParameterizedTest
    @CsvSource({
            "C12345, true",
            "T12345, false"
    })
    void isCommercialTest(String matricule, boolean expected) {
        // Given
        val employe = Employe.builder().matricule(matricule).build();
        // When / Then
        assertEquals(expected, employe.isCommercial());
    }

    @ParameterizedTest
    @CsvSource({
          "1, 26",
          "4, 29"
    })
    void getNbJoursConges(int anneeAnciennete, int expectedNbJours) {
        // Given
        val employe = Employe.builder().dateEmbauche(LocalDate.now().minusYears(anneeAnciennete)).build();
        // When
        val nbConges = employe.getNbConges(LocalDate.now()); // FIXME: would fail if the CI runs at midnight the 1 january
        // Then
        assertEquals(expectedNbJours, nbConges, "Le nombre de congees correspond au nombre d'annee d'anciennete + 25");
    }

    @ParameterizedTest
    @CsvSource({
            "0, 1000",
            "-1, 1000"
    })
    void augmenterSalaireThrows(
            // Given
            double taux,
            double salaire
    ) {
        val employe = Employe.builder().salaire(salaire).build();
        // When
        assertThatThrownBy(() -> employe.augmenterSalaire(taux))
        // Then
            .isInstanceOf(IllegalArgumentException.class)
            .hasMessage("Le pourcentage doit être supérieur à 0");
    }

    @Test
    void augmenterSalaire() {
        // Given
        val employe = Employe.builder().salaire(100.0).build();
        // When
        employe.augmenterSalaire(1.0);
        // Then
        assertThat(employe.getSalaire()).isEqualTo(200.0);
    }

    @ParameterizedTest()
    @CsvSource({
            "2019-10-10, 8",
            "2021-04-05, 10",
            "2022-03-09, 10",
            "2016-01-01, 9",
            "2032-01-27, 11",
    })
    void testgetNbRtt(LocalDate date, int expectedRtt){
        //given
        val emp = Employe.builder()
                .matricule("M12345")
                .dateEmbauche(LocalDate.now().minusYears(2))
                .salaire(Entreprise.SALAIRE_BASE)
                .performance(Entreprise.PERFORMANCE_BASE)
                .tempsPartiel(1.0)
                .build();
        //when
        int nbRtt = emp.getNbRtt(date);
        //then
        assertEquals(expectedRtt, nbRtt);
    }


}