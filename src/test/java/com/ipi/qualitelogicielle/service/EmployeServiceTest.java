package com.ipi.qualitelogicielle.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import javax.persistence.EntityExistsException;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import com.ipi.qualitelogicielle.exception.EmployeException;
import com.ipi.qualitelogicielle.model.Employe;
import com.ipi.qualitelogicielle.model.NiveauEtude;
import com.ipi.qualitelogicielle.model.Poste;
import com.ipi.qualitelogicielle.repository.EmployeRepository;
import lombok.val;

@ExtendWith(MockitoExtension.class)
class EmployeServiceTest {
    @InjectMocks
    private EmployeService employeService;

    @Mock
    private EmployeRepository employeRepository;

    @BeforeEach
    void setup() {
        MockitoAnnotations.initMocks(getClass());
    }

    @Test
    void testEmbaucheEmployeTechnicienPleinTempsBts() throws EmployeException {
        //Given
        String nom = "Doe";
        String prenom = "John";
        Poste poste = Poste.TECHNICIEN;
        NiveauEtude niveauEtude = NiveauEtude.BTS_IUT;
        Double tempsPartiel = 1.0;
        when(employeRepository.findLastMatricule()).thenReturn(Optional.of("00345"));
        when(employeRepository.findByMatricule("T00346")).thenReturn(Optional.empty());

        //When
        employeService.embaucheEmploye(nom, prenom, poste, niveauEtude, tempsPartiel);

        //Then
        ArgumentCaptor<Employe> employeArgumentCaptor = ArgumentCaptor.forClass(Employe.class);
        verify(employeRepository, times(1)).save(employeArgumentCaptor.capture());
        assertEquals(nom, employeArgumentCaptor.getValue().getNom());
        assertEquals(prenom, employeArgumentCaptor.getValue().getPrenom());
        assertEquals(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")), employeArgumentCaptor.getValue().getDateEmbauche().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        assertEquals("T00346", employeArgumentCaptor.getValue().getMatricule());
        assertEquals(tempsPartiel, employeArgumentCaptor.getValue().getTempsPartiel());

        //1521.22 * 1.2 * 1.0
        assertEquals(1825.46, employeArgumentCaptor.getValue().getSalaire().doubleValue());
    }

    @Test
    void testEmbaucheEmployeManagerMiTempsMaster() throws EmployeException {
        //Given
        String nom = "Doe";
        String prenom = "John";
        Poste poste = Poste.MANAGER;
        NiveauEtude niveauEtude = NiveauEtude.MASTER;
        Double tempsPartiel = 0.5;
        when(employeRepository.findLastMatricule()).thenReturn(Optional.of("00345"));
        when(employeRepository.findByMatricule("M00346")).thenReturn(Optional.empty());

        //When
        employeService.embaucheEmploye(nom, prenom, poste, niveauEtude, tempsPartiel);

        //Then
        ArgumentCaptor<Employe> employeArgumentCaptor = ArgumentCaptor.forClass(Employe.class);
        verify(employeRepository, times(1)).save(employeArgumentCaptor.capture());
        assertEquals(nom, employeArgumentCaptor.getValue().getNom());
        assertEquals(prenom, employeArgumentCaptor.getValue().getPrenom());
        assertEquals(LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")), employeArgumentCaptor.getValue().getDateEmbauche().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
        assertEquals("M00346", employeArgumentCaptor.getValue().getMatricule());
        assertEquals(tempsPartiel, employeArgumentCaptor.getValue().getTempsPartiel());

        //1521.22 * 1.4 * 0.5
        assertEquals(1064.85, employeArgumentCaptor.getValue().getSalaire().doubleValue());
    }

    @Test
    void testEmbaucheEmployeManagerMiTempsMasterNoLastMatricule() throws EmployeException {
        //Given
        String nom = "Doe";
        String prenom = "John";
        Poste poste = Poste.MANAGER;
        NiveauEtude niveauEtude = NiveauEtude.MASTER;
        Double tempsPartiel = 0.5;
        when(employeRepository.findLastMatricule()).thenReturn(Optional.empty());
        when(employeRepository.findByMatricule("M00001")).thenReturn(Optional.empty());

        //When
        employeService.embaucheEmploye(nom, prenom, poste, niveauEtude, tempsPartiel);

        //Then
        ArgumentCaptor<Employe> employeArgumentCaptor = ArgumentCaptor.forClass(Employe.class);
        verify(employeRepository, times(1)).save(employeArgumentCaptor.capture());
        assertEquals("M00001", employeArgumentCaptor.getValue().getMatricule());
    }

    @Test
    void testEmbaucheEmployeManagerMiTempsMasterExistingEmploye() {
        //Given
        String nom = "Doe";
        String prenom = "John";
        Poste poste = Poste.MANAGER;
        NiveauEtude niveauEtude = NiveauEtude.MASTER;
        Double tempsPartiel = 0.5;
        when(employeRepository.findLastMatricule()).thenReturn(Optional.empty());
        when(employeRepository.findByMatricule("M00001")).thenReturn(Optional.of(new Employe()));

        //When/Then
        EntityExistsException e = Assertions.assertThrows(EntityExistsException.class, () -> employeService.embaucheEmploye(nom, prenom, poste, niveauEtude, tempsPartiel));
        assertEquals("L'employé de matricule M00001 existe déjà en BDD", e.getMessage());
    }

    @Test
    void testEmbaucheEmployeManagerMiTempsMaster99999() {
        //Given
        String nom = "Doe";
        String prenom = "John";
        Poste poste = Poste.MANAGER;
        NiveauEtude niveauEtude = NiveauEtude.MASTER;
        Double tempsPartiel = 0.5;
        when(employeRepository.findLastMatricule()).thenReturn(Optional.of("99999"));

        //When/Then
        EmployeException e = Assertions.assertThrows(EmployeException.class, () -> employeService.embaucheEmploye(nom, prenom, poste, niveauEtude, tempsPartiel));
        assertEquals("Limite des 100000 matricules atteinte !", e.getMessage());
    }

    @Test
    void calculPerformanceCommercial() throws EmployeException {
        // Given
        val matricule = "C12345";
        val caTraite = 2L;
        val objectifCA = 1L;
        val employeBase = Employe.builder()
                .matricule("C12345")
                .salaire(100.0)
                .performance(1)
                .build();
        // When
        when(employeRepository.findByMatricule(matricule)).thenReturn(Optional.of(employeBase));
        when(employeRepository.save(any(Employe.class))).thenAnswer(returnsFirstArg());
        val employe = employeService.calculPerformanceCommercial(matricule, caTraite, objectifCA);
        // Then
        assertThat(employe.getPerformance()).isEqualTo(5);
    }

    @Test
    void calculPerformanceCommercialThrowsWhenAskingForNonCommercial() {
        // Given
        val matricule = "T12345";
        val caTraite = 1L;
        val objectifCa = 1L;
        // When
        // Then
        assertThatThrownBy(() -> employeService.calculPerformanceCommercial(matricule, caTraite, objectifCa))
                .isInstanceOf(EmployeException.class)
                .hasMessage("Le matricule ne peut être null et doit commencer par un C !");
    }

    @Test
    void calculPerformanceCommercialThrowsWithNegativCA() {
        // Given
        val matricule = "T12345";
        val caTraite = -1L;
        val objectifCa = 1L;
        // When
        // Then
        assertThatThrownBy(() -> employeService.calculPerformanceCommercial(matricule, caTraite, objectifCa))
                .isInstanceOf(EmployeException.class)
                .hasMessage("Le chiffre d'affaire traité ne peut être négatif ou null !");
    }

    @Test
    void calculPerformanceCommercialThrowsWithNegativObjectifCA() {
        // Given
        val matricule = "T12345";
        val caTraite = 1L;
        val objectifCa = -1L;
        // When
        // Then
        assertThatThrownBy(() -> employeService.calculPerformanceCommercial(matricule, caTraite, objectifCa))
                .isInstanceOf(EmployeException.class)
                .hasMessage("L'objectif de chiffre d'affaire ne peut être négatif ou null !");
    }

    @Test
    void calculPerformanceCommercialThrowsWithUnknownEmploye() {
        // Given
        val matricule = "C12345";
        val caTraite = 1L;
        val objectifCa = 1L;
        // When
        when(employeRepository.findByMatricule(matricule)).thenReturn(Optional.empty());
        // Then
        assertThatThrownBy(() -> employeService.calculPerformanceCommercial(matricule, caTraite, objectifCa))
                .isInstanceOf(EmployeException.class)
                .hasMessage("Le matricule " + matricule + " n'existe pas !");
    }

    @ParameterizedTest
    @CsvSource({
            "0, 1, 0",
            "1, 0, 1"
    })
    void getCommercialPerformanceBonus(int employePerformance, double avgPerf, int expectedPerf) {
        // Given
        // When
        when(employeRepository.avgPerformanceWhereMatriculeStartsWith("C")).thenReturn(Optional.of(avgPerf));
        val perfBonus = employeService.getCommercialPerformanceBonus(employePerformance);
        // Then
        assertThat(perfBonus).isEqualTo(expectedPerf);
    }

    @ParameterizedTest
    @CsvSource({
            "85, 100, 10, 8",
            "100, 100, 10, 10",
            "110, 100, 10, 11",
            "121, 100, 10, 14",
            "75, 100, 10, 1"
    })
    void getBasePerformance(long caTraite, long objectifCa, int employePerformance, int expectedResult) {
        // Given
        // When
        val basePerformance = employeService.getBasePerformance(caTraite, objectifCa, employePerformance);
        // Then
        assertEquals(basePerformance, expectedResult);
    }
}